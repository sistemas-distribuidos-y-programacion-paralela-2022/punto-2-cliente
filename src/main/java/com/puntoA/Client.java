package com.puntoA;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import com.google.gson.Gson;

public class Client {
    // Muestro un menu
    // Armo el json para hacer la consulta de tipo Message
    Scanner scanner;
    private Gson gson;

    public static void main(String[] args) {
        Client c = new Client();
        c.menu();
    }

    public Client() {
        this.scanner = new Scanner(System.in);
        this.gson = new Gson();
    }

    public void menu() {
        boolean b = true;
        while (b) {
            System.out.println("Ingrese la opcion que desee");
            System.out.println("1 - Deposito");
            System.out.println("2 - Extraccion");
            System.out.println("0 - Salir");
            String opcion = this.scanner.nextLine();
            String monto;
            Message mensaje;
            String msgJson;
            switch (opcion) {
                case "1":
                    System.out.println("Ingrese el monto");
                    monto = this.scanner.nextLine();
                    // Creo un json de tipo putDeposito
                    mensaje = new Message("putDeposito", monto);
                    msgJson = gson.toJson(mensaje);
                    enviar(msgJson);
                    break;
                case "2":
                    System.out.println("Ingrese el monto");
                    monto = this.scanner.nextLine();
                    mensaje = new Message("putExtraccion", monto);
                    msgJson = gson.toJson(mensaje);
                    enviar(msgJson);
                    // Creo un json de tipo putExtraccion
                    break;
                case "0":
                    b = false;
                    break;
                default:
                    break;
            }
        }
    }

    private void enviar(String msgJson) {
        try {
            int port = Integer.valueOf(System.getenv("PUERTO_SERVIDOR"));  
            String ipServer = System.getenv("IP_SERVIDOR");   
            System.out.println(("Conectando al servidor "+ipServer+":"+port));

            Socket socket = new Socket(ipServer, port);
            BufferedReader canalEntrada = new BufferedReader (new InputStreamReader (socket.getInputStream()));
            PrintWriter canalSalida = new PrintWriter (socket.getOutputStream(), true);
            canalSalida.println(msgJson);
            Message msgRespuesta = gson.fromJson(canalEntrada.readLine(), Message.class);
            System.out.println("Respuesta del server "  + msgRespuesta.header + ". Saldo actual: "+ msgRespuesta.bodyJSON);
            socket.close();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
